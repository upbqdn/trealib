//
// Created by marek on 4/4/18.
//

#include <random>

namespace impl {
    template<typename Item>
    Node<Item>::Node(const Item &t_i) :
            i{t_i},
            l{nullptr},
            r{nullptr} {
        prior = rng();
    }

    template<typename Item>
    Node<Item>::~Node() {
    }

    template<typename Item>
    Node <Item> *
    Node<Item>::rotL() {
        Node<Item> *tmp = r;
        r = r->l;
        tmp->l = this;

        return tmp;
    }

    template<typename Item>
    Node <Item> *
    Node<Item>::rotR() {
        Node<Item> *tmp = l;
        l = l->r;
        tmp->r = this;

        return tmp;
    }

    template<typename Item>
    void
    Node<Item>::delSubTree() {

        if (l) {
            l->delSubTree();
            l = nullptr;
        }

        if (r) {
            r->delSubTree();
            r = nullptr;
        }

        delete this;
    }

    rng::operator unsigned() const{
        using namespace std;

        static random_device rd;
        static mt19937 rng(rd());
        // -1 represents the max value of the unsigned int
        static uniform_int_distribution<unsigned> unif_rng(0, -1);

        return unif_rng(rng);
    }
}
