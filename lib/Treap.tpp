//
// Created by marek on 4/4/18.
//

#include <random>

template<typename Key, typename Val, typename Cmp>
Treap<Key, Val, Cmp>::Treap() : m_size(0), m_root(nullptr) {
}

template<typename Key, typename Val, typename Cmp>
Treap<Key, Val, Cmp>::Treap(std::initializer_list<Item> t_il) : Treap() {
    for (const auto i : t_il) {
        insert(i);
    }
}

template<typename Key, typename Val, typename Cmp>
const std::pair<Key, Val> *
Treap<Key, Val, Cmp>::insert(const Item &t_i) {
    using namespace impl;
    using namespace std;

    auto *n = new N(t_i);
    m_tmp_i = nullptr;
    m_root = insert(n, m_root);

    return m_tmp_i;
}

template<typename Key, typename Val, typename Cmp>
bool
Treap<Key, Val, Cmp>::erase(const Key &t_k) {
    m_succ = false;
    m_root = erase(t_k, m_root);

    return m_succ;
}

template<typename Key, typename Val, typename Cmp>
void
Treap<Key, Val, Cmp>::clear() {
    m_root->delSubTree();
    m_root = nullptr;
    m_size = 0;
}

template<typename Key, typename Val, typename Cmp>
unsigned
Treap<Key, Val, Cmp>::size() const {
    return m_size;
}

template<typename Key, typename Val, typename Cmp>
const std::pair<Key, Val> *
Treap<Key, Val, Cmp>::search(const Key &t_k) const {
    using namespace impl;

    auto *n = search(t_k, m_root);

    return n ? &n->i : nullptr;
}

template<typename Key, typename Val, typename Cmp>
bool
Treap<Key, Val, Cmp>::empty() const {
    return m_size == 0;
}

template<typename Key, typename Val, typename Cmp>
Treap<Key, Val, Cmp>::~Treap() {
    if (m_root) {
        m_root->delSubTree();
    }

    m_root = nullptr;
}

template<typename Key, typename Val, typename Cmp>
impl::Node<std::pair<Key, Val>> *
Treap<Key, Val, Cmp>::insert(N *t_n, N *t_t) {
    //insert the new node
    if (!t_t) {
        ++m_size;
        m_tmp_i = &t_n->i;
        return t_n;
    }

    //go to the left branch
    if (m_cmp(t_n->i.first, t_t->i.first)) {
        t_t->l = insert(t_n, t_t->l);
        if (t_t->prior > t_t->l->prior) {
            t_t = t_t->rotR();
        }
        //go to the right branch
    } else if (m_cmp(t_t->i.first, t_n->i.first)) {
        t_t->r = insert(t_n, t_t->r);
        if (t_t->prior > t_t->r->prior) {
            t_t = t_t->rotL();
        }
        //keys match, update the node
    } else {
        t_t->i.second = t_n->i.second;
        delete t_n;
        m_tmp_i = &t_t->i;
    }

    return t_t;
}

template<typename Key, typename Val, typename Cmp>
impl::Node<std::pair<Key, Val>> *
Treap<Key, Val, Cmp>::search(const Key &t_k, N *t_t) const {

    if (!t_t) {
        return nullptr;
    }

    if (m_cmp(t_k, t_t->i.first)) {
        return search(t_k, t_t->l);
    } else if (m_cmp(t_t->i.first, t_k)) {
        return search(t_k, t_t->r);
    } else {
        return t_t;
    }
}

template<typename Key, typename Val, typename Cmp>
impl::Node<std::pair<Key, Val>> *
Treap<Key, Val, Cmp>::erase(N *t_n) {
    using namespace impl;

    N *tmp;

    if (!t_n->l) {
        tmp = t_n->r;
        delete t_n;
        return tmp;
    }

    if (!t_n->r) {
        tmp = t_n->l;
        delete t_n;
        return tmp;
    }

    if (m_cmp(t_n->l->prior, t_n->r->prior)) {
        tmp = t_n->rotR();
        tmp->r = erase(t_n);
    } else {
        tmp = t_n->rotL();
        tmp->l = erase(t_n);
    }

    return tmp;
}

template<typename Key, typename Val, typename Cmp>
impl::Node<std::pair<Key, Val>> *
Treap<Key, Val, Cmp>::erase(const Key &t_k, N *t_t) {
    if (!t_t) {
        return nullptr;
    }

    if (m_cmp(t_k, t_t->i.first)) {
        t_t->l = erase(t_k, t_t->l);
    } else if (m_cmp(t_t->i.first, t_k)) {
        t_t->r = erase(t_k, t_t->r);
    } else {
        --m_size;
        m_succ = true;
        return erase(t_t);
    }

    return t_t;
}

template<typename Key, typename Val, typename Cmp>
const std::pair<Key, Val> *
Treap<Key, Val, Cmp>::min() const {
    const auto *n = min(m_root);

    return n ? &n->i : nullptr;
}

template<typename Key, typename Val, typename Cmp>
const std::pair<Key, Val> *
Treap<Key, Val, Cmp>::max() const {
    const auto *n = max(m_root);

    return n ? &n->i : nullptr;
}

template<typename Key, typename Val, typename Cmp>
const impl::Node<std::pair<Key, Val>> *
Treap<Key, Val, Cmp>::min(const N *t_n) const {
    return t_n ? (t_n->l ? min(t_n->l) : t_n) : nullptr;
}

template<typename Key, typename Val, typename Cmp>
const impl::Node<std::pair<Key, Val>> *
Treap<Key, Val, Cmp>::max(const N *t_n) const {
    return t_n ? (t_n->r ? max(t_n->r) : t_n) : nullptr;
}

template<typename Key, typename Val, typename Cmp>
const impl::Node<std::pair<Key, Val>> *
Treap<Key, Val, Cmp>::getNext(const N *t_n) {
    if (t_n->r) {
        return min(t_n->r);
    }

    const N *n = nullptr;
    const N *r = m_root;

    while (r) {
        if (m_cmp(t_n->i.first, r->i.first)) {
            n = r;
            r = r->l;
        } else if (m_cmp(r->i.first, t_n->i.first)) {
            r = r->r;
        } else {
            break;
        }
    }

    return n;
}

template<typename Key, typename Val, typename Cmp>
const impl::Node<std::pair<Key, Val>> *
Treap<Key, Val, Cmp>::getPrev(const N *t_n) {
    if (!t_n) {
        return max(m_root);
    }

    if (t_n->l) {
        return max(t_n->l);
    }

    N *p = nullptr;
    N *r = m_root;

    while (r) {
        if (m_cmp(r->i.first, t_n->i.first)) {
            p = r;
            r = r->r;
        } else if (m_cmp(t_n->i.first, r->i.first)) {
            r = r->l;
        } else {
            break;
        }
    }

    return p;
}

template<typename Key, typename Val, typename Cmp>
impl::Iterator<std::pair<Key, Val>, Treap<Key, Val, Cmp>>
Treap<Key, Val, Cmp>::begin() {
    return impl::Iterator<Item, Treap<Key, Val, Cmp>>(min(m_root), this);
}

template<typename Key, typename Val, typename Cmp>
impl::Iterator<std::pair<Key, Val>, Treap<Key, Val, Cmp>>
Treap<Key, Val, Cmp>::end() {
    return impl::Iterator<Item, Treap<Key, Val, Cmp>>(nullptr, this);
}

template<typename Key, typename Val, typename Cmp>
const std::pair<Key, Val> *
Treap<Key, Val, Cmp>::operator[](Key t_k) const {
    return search(t_k);
}

template<typename Key, typename Val, typename Cmp>
typename Treap<Key, Val, Cmp>::TreapProxy
Treap<Key, Val, Cmp>::operator[](const Key &t_k) {
    return TreapProxy(this, t_k);
}
