//
// Created by marek on 4/8/18.
//

#ifndef TREAP_ITERATOR_HPP
#define TREAP_ITERATOR_HPP

#include <cstddef>
#include <iterator>
#include <vector>
#include "Node.hpp"

/**
 * This namespace contains supporting functionality for the Treap class,
 * it is not intended for a direct client use.
 */
namespace impl {
    /**
     * Iterator for the Treap class.
     * @tparam Item Type of objects that the Treap stores.
     * @tparam Treap Type of the Treap that the Iterator is bound to.
     */
    template<typename Item, typename Treap>
    class Iterator {
        using value_type = Item;
        using difference_type = std::ptrdiff_t;
        using pointer = Item *;
        using iterator_category = std::bidirectional_iterator_tag;

    public:
        /**
         * Dereference operator.
         * @return Reference to an Item that the Iterator currently points to.
         * \par Complexity
         * constant
         */
        const Item &
        operator*() const;

        /**
         * Dereference operator.
         * @return Pointer to an Item that the Iterator currently points to.
         * \par Complexity
         * constant
         */
        const Item *
        operator->() const;

        /**
         * Pre-increment.
         * Moves the inner pointer of the Iterator to the following Item in the
         * Treap.
         * @return Reference to the incremented Iterator.
         * \par Complexity
         * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
         */
        Iterator<Item, Treap> &
        operator++();

        /**
         * Post-increment.
         * Moves the inner pointer of the Iterator to the following Item in the
         * Treap.
         * @return Copy of the original Iterator that hasn't been incremented.
         * \par Complexity
         * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
         */
        Iterator<Item, Treap>
        operator++(int);

        /**
         * Pre-decrement.
         * Moves the inner pointer of the Iterator to the previous Item in the
         * Treap.
         * @return Reference to the decremented Iterator.
         * \par Complexity
         * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
         */
        Iterator<Item, Treap> &
        operator--();

        /**
         * Post-decrement.
         * Moves the inner pointer of the Iterator to the previous Item in the
         * Treap.
         * @return Copy of the original Iterator that hasn't been decremented.
         * \par Complexity
         * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
         */
        Iterator<Item, Treap>
        operator--(int);

        /**
         * Compare two Iterators for equality.
         * @param rhs Second Iterator for the comparison.
         * @return true if both Iterators point to an Item with the same value
         * of the key, false otherwise
         * \par Complexity
         * constant
         */
        bool
        operator==(const Iterator<Item, Treap> &rhs);

        /**
         * Compare two Iterators for inequality.
         * @param rhs Second Iterator for the comparison.
         * @return true if the Iterators point to Items with different value of
         * the key, false otherwise
         * \par Complexity
         * constant
         */
        bool
        operator!=(const Iterator<Item, Treap> &rhs);

    private:
        const Node<Item> *m_n;
        Treap *m_t;

        Iterator(const Node<Item> *t_n, Treap *t_t);

        friend Treap;
    };
}

#include "Iterator.tpp"

#endif //TREAP_ITERATOR_HPP
