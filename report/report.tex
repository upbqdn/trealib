%%
%% Author: marek
%% 5/6/18
%%

% Preamble
\documentclass[11pt]{article}

% Packages
\usepackage{a4wide}
\usepackage{float}
\usepackage{booktabs}
\usepackage{amsmath}

\newcommand{\BigO}[1]{\ensuremath{\operatorname{O}\left(#1\right)}}

\title{Treaps} % Title

\author{Marek Bielik} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\section{General overview}
A treap is a data structure that has a form of a self-balanced
BST\footnote{Binary Search Tree} and therefore it is suitable for
storing ordered data in a more effective way than a standard BST.

Every node contains a priority number (randomly generated), key (must
be unique) and optionally a value that the key represents.  Nodes in a
treap follow the heap-ordering property considering the priority
number of the node, that is: the priority of any non-leaf node is
greater or equal to the priority of its descendants.  If we take into
consideration only the keys, nodes in the treap follow the order of a
standard BST, that means that the value of the key of the left
descendant is lower than the value of the key of the current node and
the value of the key of the right descendant is higher than the value
of the key of the current node.  The word treap is a blend of the
words tree and heap.

The fact that the priorities are generated randomly ensures that with
high probability the height of the treap is proportional to the
logarithm of the number of its nodes $n$ and therefore; the standard
search, insert and delete operations have logarithmic time complexity
with high probability as well.  Table \ref{comp-tab} summarizes the
time complexity of the operations.  The space complexity of a treap is
$O(n)$.

\begin{table}[H]
  \centering
  \begin{tabular}{ccc}
    \toprule
    \multicolumn{1}{c}{} & \multicolumn{2}{c}{Complexity} \\
    \cline{2-3}
    Operation & Average & Worst Case \\
    \midrule
    Insert & $O(\log n)$ & $O(n)$ \\
    Search & $O(\log n)$ & $O(n)$  \\
    Delete & $O(\log n)$ & $O(n)$  \\
    Min. \& max. element & $O(\log n)$ & $O(n)$ \\
    Next \& prev. element & $O(\log n)$ & $O(n)$ \\
    \bottomrule
  \end{tabular}
  \caption{Time complexity of the standard operations for the Treap
    data structure.}
  \label{comp-tab}
\end{table}


\section{Operations}
The following subsections contain a detailed description of the
standard treap operations. The time complexity of every operation
depends on the shape of the treap. In the worst case, the treap can
have a form of a linked list which means that every operation will
require time that is linearly proportional to the number of nodes, so
the time complexity will become $O(n)$. This could happen if we were
storing a growing sequence of keys and if we had a poor generator that
would also generate a growing sequence of priorities.

However, with high probability (providing we have a solid generator of
random numbers for the priorities) the treap will form a balanced BST
as mentioned in the previous section.  We could even store a growing
sequence of keys.
    
\subsection{Insert}
In order to insert a new node into the treap, first we randomly
generate its priority. Then we use the search operation described in
section \ref{search-subsec} in order to determine a new leaf position
for the node and we insert it. If the keys match during the search, it
is not possible to insert the new node.

If the newly inserted node has a higher priority than its predecessor,
we need to perform rotations (described in the following subsection)
until the heap property of the treap is satisfied again.  We use the
left rotation for right descendants and the right rotation for left
descendants.  This will bring the node up by one level in the treap
and we need to continue with the rotations until the heap property is
satisfied i.e. every parent node has a higher priority than its
descendants.

The approach described above is the key functionality that makes the
shape of the treap balanced and therefore provides the logarithmic
properties of the treap.

\subsubsection{Rotations in a treap}
Rotations of nodes in a treap are performed in the same way as in a
standard BST. The left rotation can be done in the following way:
Consider a pointer to the current node which has two descendants. We
store the value of the pointer to the right descendant into a
temporary variable and after that we set that pointer to point to the
left descendant of the right descendant of the current node. Then we
set the pointer that points to the left descendant of the temporarily
stored pointer to point to the current node and we return the
temporarily stored pointer.

The pointer that we returned represents the new current node after the
left rotation. Right rotations can be performed in the same way with
inverse logic.  Both the left and right rotations take constant time.

\subsection{Search}
\label{search-subsec}
In order to perform the search operation, we apply the standard binary
search algorithm ignoring the priorities.

We start in the root and compare its key to the key that we are
looking for. If the searched key is greater than the key of the root,
we set the right descendant to be the new root node. If the searched
key is smaller than the key of the root, we set the left descendant to
be the new node, otherwise - when the keys match - the root node is
the one that we are looking for. If there is no descendant node to be
set as the new root, there is no such key in the treap.

\subsubsection{Get the min. and max. element}
We can obtain the minimal element of the treap by simply jumping to
the left descendant of the root node until there is no descendant to
jump to. The last node without the left child is the minimal element
of the treap. We can ignore both the priorities and key values.  If we
want to obtain the maximal element, we need to choose the right
descendant instead.

\subsubsection{Get the next and previous element}
The operations for getting the next and previous elements are
complementary to each other as well as rotations or the operations for
getting the minimal and maximal element.  We can obtain the next
element of a node X by finding its in-order successor in the treap: If
X has its right descendant, the next element is the minimal element of
the treap in which the right descendant represents the root node.  If
X has no right descendant, the next node is one of the predecessors,
and we need to start searching from the root node of the whole treap
(providing we don't use pointers to predecessor nodes).  In this case,
we come down the tree and we compare the keys of X and the current
root node.  If X has lower key than the current root node, we choose
the left descendant of the current root to be the new current root and
we also store this descendant into a temporary variable.  If X has
higher key, we choose the right descendant of the current root to be
the new root node and we don't store it this time.  We repeat the
these steps until the keys match or until there is no node to be
chosen. The node stored in the temporary variable is the next node
after X.

These operations may be used to iterate over the treap.  Since the
average time complexity to get the next node is $O(\log n)$, the
overall time complexity of the whole iteration is $O(n \log n)$,
providing the treap has $n$ nodes.

This approach is also dynamic which means that we can pause the
iteration, add or delete any number of nodes, and continue iterating
over the new state of the treap.

\subsection{Delete}
If the node being deleted is a leaf node, we can simply remove it from
the treap.  If the node has one descendant, we can remove the node and
put the descendant at the place of the removed node.  If the node
being deleted has two descendants, we need to bring this node all the
way down the treap until it has only one or none descendants and then
we can remove it by following the previous cases.  We can bring the
node down by using rotations again: If the left descendant has a
higher priority than the right one, we use the right rotation,
otherwise we use the left rotation.  This will bring the node intended
for deletion down by one level. We need to continue with the rotations
until we can delete the node.

% \begin{figure}[h]
%   \begin{center}
%     \includegraphics[width=0.65\textwidth]{placeholder} % Include the image placeholder.png
%     \caption{Figure caption.}
%   \end{center}
% \end{figure}

% \bibliography{sample}

\end{document}