//
// Created by marek on 4/4/18.
//

#ifndef TREAP_TREAP_HPP
#define TREAP_TREAP_HPP

#include "Node.hpp"
#include "Iterator.hpp"
#include <functional>

/**
 * Associative container.
 * Treaps are associative containers that store elements formed by a
 * combination of a key value and a mapped value, following a specific order.
 * Treaps combine trees and heaps.
 * @tparam Key Type of the keys.
 * Each element in a Treap is uniquely identified by its key value.
 * @tparam Val Type of the mapped value.
 * @tparam Cmp The Treap object uses this expression to determine both the
 * order the elements follow in the container and whether two element keys are
 * equivalent (by comparing them reflexively: they are equivalent if !comp(a,b)
 * && !comp(b,a)). No two elements in a Treap can have equivalent
 * keys. This defaults to less<T>, which returns the same as applying the
 * less-than operator (a<b).
 */
template<typename Key, typename Val, typename Cmp = std::less<Key>>
class Treap {
public:
    /**
     * Elements of this type are stored in the Treap.
     */
    typedef std::pair<Key, Val> Item;

    /**
     * Proxy class for the overloaded operator[].
     * This class is used as a proxy for the original Treap class in order to
     * utilize the operator[] both as a getter and setter.
     */
    class TreapProxy {
    public:
        /**
         * Casting operator (getter).
         * This operator is used as a getter for the original Treap class.
         * @return A pointer to an Item with the corresponding key, nullptr if
         * there was no such Item.
         * @see search()
         * \par Complexity
         * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$*/
        operator const Item *() const { return m_t->search(m_k); }

        /**
         * Assignment operator (setter).
         * This operator is used as a setter for the original Treap class.
         * @param t_v Value to be stored in the Item.
         * @return Pointer to the newly inserted Item or to an already
         * existing updated Item.
         * @see insert()
         * \par Complexity
         * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
         */
        const Item *operator=(const Val &t_v) {
            return m_t->insert(std::pair<Key, Val>(m_k, t_v));
        }

    private:
        TreapProxy(Treap<Key, Val, Cmp> *t_t, const Key &t_k) :
                m_t(t_t),
                m_k(t_k) {}

        Treap<Key, Val, Cmp> *m_t;
        const Key &m_k;

        friend Treap;
    };

    /**
     * Iterator for the Treap class.
     */
    using iterator = impl::Iterator<Item, Treap<Key, Val, Cmp>>;

    /**
     * Iterator for the Treap class.
     */
    friend iterator;

    /**
     * Iterator referring to the first element in the Treap.
     * If the treap is empty, the returned iterator value will be equal to
     * end() and should not be dereferenced.
     * @return Iterator referring to the first element in the Treap.
     * \par Complexity
     * constant
     */
    iterator begin();

    /**
     * Iterator referring to the past-the-end element in the Treap.
     * The past-the-end element is the theoretical element that would follow
     * the last element and thus shall not be dereferenced.
     * @return Iterator referring to the past-the-end element in the Treap.
     * \par Complexity
     * constant
     */
    iterator end();

    /**
     * Constructs a Treap object.
     * \par Complexity
     * constant
     */
    Treap();

    /**
     * Constructs a Treap object with initial elements.
     * @param t_il List of elements initializing the contents of the Treap.
     * \par Complexity
     * average: \f$ O(n \log n) \f$, worst case: \f$ O(n^2) \f$
     */
    Treap(std::initializer_list<Item> t_il);

    /**
     * Inserts or updates an Item in the Treap.
     *
     * @param t_i Item to be inserted into the Treap. If the Treap already
     * contains an Item with the same key, its value will be updated with the
     * value from t_i.
     * @return Pointer to the newly inserted Item or to an already
     * existing Item that has been updated.
     * \par Complexity
     * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
     */
    const Item *
    insert(const Item &t_i);

    /**
     * Deletes an Item from the Treap.
     * @param t_k Key of the Item to be deleted.
     * @return True if there was an Item that was deleted, false otherwise.
     * \par Complexity
     * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
     */
    bool
    erase(const Key &t_k);

    /**
     * Search for an Item in the Treap.
     * @param t_k Key to be searched for.
     * @return Pointer to an Item with the corresponding key, nullptr if
     * there was no such Item.
     * \par Complexity
     * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
     */
    const Item *
    search(const Key &t_k) const;

    /**
     * Get minimal Item.
     * @return Pointer to the Item in the Treap with the minimal key value.
     * \par Complexity
     * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
     */
    const Item *
    min() const;

    /**
     * Get maximal Item.
     * @return Pointer to the Item in the Treap with the maximal key value.
     * \par Complexity
     * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
     */
    const Item *
    max() const;

    /**
     * Deletes all the Items in the Treap.
     * \par Complexity
     * average: \f$ O(n) \f$
     */
    void
    clear();

    /**
     * Get the number of Items in the Treap.
     * @return The number of Items in the Treap.
     * \par Complexity
     * constant
     */
    unsigned
    size() const;

    /**
     * Test whether the Treap is empty.
     * @return true if the Treap is empty, false otherwise.
     * \par Complexity
     * constant
     */
    bool
    empty() const;

    /**
     * Destructs the Treap.
     * \par Complexity
     * average: \f$ O(n) \f$
     */
    ~Treap();

    /**
     * This operator can be used both as a getter and setter.
     * The appropriate operation is performed by the TreapProxy class.
     * @param t_k Key to be searched for.
     * @return Proxy class that implements the 'get' and 'set' operations by
     * overloading the subscript and assignment operators, respectively.
     * @see TreapProxy
     * \par Complexity
     * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
     */
    TreapProxy
    operator[](const Key &t_k);

    /**
     * A getter for the Treap.
     * @param t_k Key to be searched for.
     * @return Pointer to an Item with the corresponding key, nullptr if
     * there was no such Item.
     * @see search()
     * \par Complexity
     * average: \f$ O(\log n) \f$, worst case: \f$ O(n) \f$
     */
    const Item *
    operator[](Key t_k) const;


private:
    typedef impl::Node<Item> N;

    unsigned m_size;
    N *m_root;
    Cmp m_cmp;
    Item *m_tmp_i;
    bool m_succ;

    N *
    insert(N *t_n, N *t_t);

    N *
    erase(const Key &t_k, N *t_t);

    N *
    erase(N *t_n);

    N *
    search(const Key &t_k, N *t_t) const;

    const N *
    min(const N *t_n) const;

    const N *
    max(const N *t_n) const;

    const N *
    getNext(const N *t_n);

    const N *
    getPrev(const N *t_n);
};

#include "Treap.tpp"

#endif //TREAP_TREAP_HPP
