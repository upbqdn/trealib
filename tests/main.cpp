#include <iostream>
#include "main.hpp"
#include "Treap.hpp"

int main() {
    using namespace std;

    //init a new treap with custom objects
    //the treap stores the objects as pairs (P is std::pair<Key, Foo>)
    //use an initializer
    Treap<Key, Foo> treap{P(4, 4), P(10, 10), P(11, 11), P(1, 1), P(2, 2)};

    //print out the contents (in a sorted order)
    //we can use iterators
    Treap<Key, Foo>::iterator it = treap.begin();
    Treap<Key, Foo>::iterator end = treap.end();

    for (; it != end; ++it) {
        cout << "(" << it->first << ", " << it->second << ") ";
    }
    cout << endl;

    //insert a new object
    //'insert' returns a pointer to the inserted object (which is const)
    const P *obj = treap.insert(P(5, 5));
    if (obj) cout << "Key: " << obj->first << " Value: " << obj->second << endl;

    //update an existing object
    //the 'insert' method can be used either to insert objects or to update them
    obj = treap.insert(P(5, 50));
    if (obj) cout << "Key: " << obj->first << " Value: " << obj->second << endl;

    //search for an object
    obj = treap.search(1);
    if (obj) cout << "Key: " << obj->first << " Value: " << obj->second << endl;

    obj = treap.search(3);
    if (!obj) cout << "No object with key: 2" << endl << endl;

    //erase an object from the treap
    cout << "Size of the treap: " << treap.size() << endl;
    if (treap.erase(2)) cout << "Erased object with key: " << 2 << endl;
    else cout << "Erased: nothing" << endl;
    cout << "Size of the treap: " << treap.size() << endl;

    //if we try to erase the same object again, 'erase' will return false
    if (treap.erase(2)) cout << "Erased: object with key: " << 2 << endl;
    else cout << "Erased: nothing" << endl;
    cout << "Size of the treap: " << treap.size() << endl << endl;

    //get min element
    obj = treap.min();
    if (obj)
        cout << "Min element: (" << obj->first << ", " << obj->second << ")"
             << endl;

    //get max element
    obj = treap.max();
    if (obj)
        cout << "Max element: (" << obj->first << ", " << obj->second << ")"
             << endl << endl;

    //we can also use a bit of syntactic sugar
    //the value in the brackets is the key, not the position of an element
    obj = treap[4];
    if (obj)
        cout << "(" << obj->first << ", " << obj->second << ")" << endl;

    obj = treap[3];
    if (!obj) cout << "No such element with key: " << 3 << endl;

    //we can also insert new elements or update the current ones
    obj = treap[3] = 3;
    if (obj)
        cout << "(" << obj->first << ", " << obj->second << ")" << endl;

    obj = treap[3] = 30;
    if (obj)
        cout << "(" << obj->first << ", " << obj->second << ")" << endl;

    //we can also iterate over the treap in a modern way
    for (const auto &i : treap) {
        cout << "(" << i.first << ", " << i.second << ") ";
    }
    cout << endl << endl;

    //clear the treap
    treap.clear();
    cout << treap.empty() << endl << endl;

    //we can use a custom comparator, if we want to store the objects
    //in a specific order
    //the following comparator redefines '<' to '>'
    Treap<Key, Foo, Comparator<Key>> treap2{P(1, 1), P(2, 2), P(3, 3), P(4, 4)};

    for (const auto &i : treap2) {
        cout << "(" << i.first << ", " << i.second << ") ";
    }
    cout << endl;

    return EXIT_SUCCESS;
}